class SectionController < ApplicationController

  def create
    section = Section.create(section_params)
    if section.errors.empty?
      render json: section
    else
      render json: { Status: 'Failed' }
    end
  end

  def update
    section = Section.find(params[:id]).update(section_params)
    render json: section
  end

  def remove
    section = Section.find(params[:id]).destroy
    render json: section

  end

  def show
    section = Section.find(params[:id])
    render json: section
  end

  def index
    section = Section.all
    render json: section
  end

  def fetch_subsections
    subsections = Section.where(parent_id: params[:id])
    render json: subsections
  end

  private

  def section_params
    params.require(:section).permit(:name, :parent_id)
  end

end
