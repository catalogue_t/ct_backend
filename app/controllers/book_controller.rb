class BookController < ApplicationController

  def create
    book = Book.create(book_params)
    if book.errors.empty?
      render json: book
    else
      render json: { Status: 'Failed' }
    end
  end

  def update
    book = Book.find(params[:id]).update(book_params)
    render json: book
  end

  def remove
    book = Book.find(params[:id]).destroy
    render json: book

  end

  def show
    book = Book.find(params[:id])
    render json: book
  end

  def index
    book = Book.all
    render json: book
  end

  def fetch_books_by_section_id
    books = Book.where(section_id: params[:id])
    render json: books
  end

  private

  def book_params
    params.require(:book).permit(:name, :section_id)
  end

end
