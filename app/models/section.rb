class Section < ApplicationRecord
  has_many :books

  # has_many :children, class_name: 'Section', foreign_key: 'parent_id'
  # belongs_to :parent, class_name: 'Section', optional: true

  validates :name, presence: true
end
