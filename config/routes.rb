Rails.application.routes.draw do

  # SECTION
  get     'sections'    => 'section#index'
  post    'section'     => 'section#create'
  get     'section/:id' => 'section#show'
  patch   'section/:id' => 'section#update'
  delete  'section/:id' => 'section#remove'
  get     'fetch_subs/:id' => 'section#fetch_subsections'
  get     'trees' => 'section#trees'

  # BOOKS
  get     'books'       => 'book#index'
  post    'book'        => 'book#create'
  get     'book/:id'    => 'book#show'
  patch   'book/:id'    => 'book#update'
  delete  'book/:id'    => 'book#remove'
  get     'fetch_books/:id' => 'book#fetch_books_by_section_id'

  # ABOUT
  get 'about' => 'about#info'

  # ROOT
  root 'about#info'
end
