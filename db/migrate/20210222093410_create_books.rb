class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.integer :section_id
      t.string :name

      t.timestamps
    end
  end
end
