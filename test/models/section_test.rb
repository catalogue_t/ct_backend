require "test_helper"

class SectionTest < ActiveSupport::TestCase
  test "should not save section without name" do
    s = Section.new
    assert_not s.save, "Saved the section without a name"
  end
end
