require "test_helper"

class SectionControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get section_new_url
    assert_response :success
  end

  test "should get update" do
    get section_update_url
    assert_response :success
  end

  test "should get remove" do
    get section_remove_url
    assert_response :success
  end

  test "should get show" do
    get section_show_url
    assert_response :success
  end
end
